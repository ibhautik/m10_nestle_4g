#!/bin/bash
echo 23 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio23/direction
echo 0 > /sys/class/gpio/gpio23/value
sleep 1
echo 1 > /sys/class/gpio/gpio23/value
sleep 2
echo 0 > /sys/class/gpio/gpio23/value
echo 23 > /sys/class/gpio/unexport