import time
import serial
import sys
import glob
import json

gFileName = "/home/debian/PDI_Info.txt"
gEnableDebug = 1
gCICCID = gIMSI = gSIMEI = gSIM_COM = gJSONPayload_A = {}

commandList = ['AT+ICCID\r\n', 'AT+CIMI\r\n','AT+SIMEI?\r\n','AT+CGMM\r\n']

responseList = ['+ICCID:','\r\n','+SIMEI:','\r\n']

def serial_ports():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/ttyU[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/ttyU.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def Search_Data(read_val_1,CommandNum):
    global gJSONPayload_A
    global Count999

    parseWordLength = 0
    responseDataSize = 0
    reponseDataIndex = 0
    if CommandNum == 0:
        if responseList[0] in read_val_1:
            if gEnableDebug == 0:
                print('Word  Found')
            parseWordLength = len(responseList[0])
            responseDataSize = 20
            reponseDataIndex = read_val_1.index(responseList[0]) + parseWordLength
            if gEnableDebug == 0:
                print (reponseDataIndex)
            gCICCID['ICCID'] = read_val_1[reponseDataIndex+1:responseDataSize+reponseDataIndex+1]
            gJSONPayload_A = json.dumps(gCICCID, sort_keys=True)
        
        else:
            if gEnableDebug == 0:
                print('Word Not Found')


    if CommandNum == 1:
        if gEnableDebug == 0:
            print('Word  Found')
        parseWordLength = len(responseList[1])
        responseDataSize = 15
        reponseDataIndex = read_val_1.index(responseList[1]) + parseWordLength
        if gEnableDebug == 0:
            print (reponseDataIndex)
        gIMSI['IMSI'] = read_val_1[reponseDataIndex:responseDataSize+reponseDataIndex]
        gJSONPayload_A = json.dumps(gIMSI, sort_keys=True)
    
    if CommandNum == 2:
        if gEnableDebug == 0:
            print('Word  Found')
        parseWordLength = len(responseList[2])
        responseDataSize = 15
        reponseDataIndex = read_val_1.index(responseList[2]) + parseWordLength
        if gEnableDebug == 0:
            print (reponseDataIndex)
        gIMSI['IMEI'] = read_val_1[reponseDataIndex+1:responseDataSize+reponseDataIndex+1]
        gJSONPayload_A = json.dumps(gIMSI, sort_keys=True)

                
    if CommandNum == 3:
        if gEnableDebug == 0:
            print('Word  Found')
        parseWordLength = len(responseList[3])
        responseDataSize = 20
        reponseDataIndex = read_val_1.index(responseList[3]) + parseWordLength
        if gEnableDebug == 0:
            print (reponseDataIndex)
        responseStr = read_val_1[reponseDataIndex:responseDataSize+reponseDataIndex]
        if gEnableDebug == 0:
            print(responseStr)
        reponseDataIndex = responseStr.find('\r\n')
        gSIM_COM['SIM_MODEL'] = responseStr[0:reponseDataIndex]
        gJSONPayload_A = json.dumps(gSIM_COM, sort_keys=True)

def writeToFile(dataToWrite):
    fd= open(gFileName,"w+")

    fd.write(dataToWrite)

    fd.close()

def checkAllUSB():
    usbList = serial_ports()
    if gEnableDebug == 0:
        print(usbList)
    loopCounter = 0
    while loopCounter < len(usbList):
        try:
            ser = serial.Serial(port=usbList[loopCounter],baudrate = 9600,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS)
            ser.isOpen() # try to open port, if possible print message and proceed with 'while True:'
            if gEnableDebug == 0:
                print ("port is opened!")
            cmd = 'ATE0\r\n'
            ser.write(cmd.encode())
            res = ''
            out = ''
            time.sleep(1)
            while ser.inWaiting() > 0:
                out += ser.read(1).decode('ascii')

            if out != '':
                if gEnableDebug == 0:
                    if gEnableDebug == 0:
                        print (">>" + out)
                if 'OK' in out:
                    #print('Success!')
                    if gEnableDebug == 0:
                        print("Found 4G Port")
                    break
                else:
                    if gEnableDebug == 0:
                        print("4G Port Not Found")

        except IOError: # if port is already opened, close it and open it again and print message
            ser.close()
            #ser.open()
            if gEnableDebug == 0:
                print ("port was already open, was closed and opened again!")
        loopCounter += 1


    input1=1
    CommandCounter = 0
    while 1 :
        ser.write(commandList[CommandCounter].encode())
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)
        while ser.inWaiting() > 0:
            out += ser.read(1).decode('ascii')

        if out != '':
            if gEnableDebug == 0:
                print (">>" + out)
            Search_Data(out,CommandCounter)
        
        CommandCounter += 1
        if CommandCounter >= len(commandList):
            ser.close()
            break

checkAllUSB()

if gJSONPayload_A:
    writeToFile(gJSONPayload_A)
    print(gJSONPayload_A)
