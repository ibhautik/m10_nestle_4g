#!/bin/bash
mkdir /usr/share/iBotGSM
cp GSM_OnBootPinConfig.sh /usr/share/iBotGSM/
cp GSM_Modem_PowerKey.sh /usr/share/iBotGSM/
cp PDI_Info.py /usr/share/iBotGSM/
cp GSM_PDI.sh /usr/share/iBotGSM/
cp GSM_BootService.service /lib/systemd/system/iBotGSMBoot.service
systemctl enable iBotGSMBoot.service
systemctl start iBotGSMBoot.service
#cp GSM_PDIService.service /lib/systemd/system/iBotGSMPDI.service
#systemctl enable iBotGSMPDI.service
#systemctl start iBotGSMPDI.servicedebian